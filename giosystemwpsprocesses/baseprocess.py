import logging
import types

from pywps import Process

logger = logging.getLogger(__name__)


class BaseProcess(Process.WPSProcess):

    def __init__(self, identifier, title, version, abstract):
        Process.WPSProcess.__init__(
            self,
            identifier=identifier,
            title=title,
            version=version,
            statusSupported=True,
            storeSupported=True,
            abstract=abstract
        )
        self._previous_percent_done = 0
        self.package_name = self.addLiteralInput(
            identifier="package",
            title="Package name",
            abstract="Name of the package to process. This name must match the"
                     " name defined in the settings",
            type=types.StringType
        )
        self.settings_url = self.addLiteralInput(
            identifier="settings",
            title="Settings URL",
            abstract="URL of the settings to use for configuring the package",
            type=types.StringType
        )
        self.timeslot = self.addLiteralInput(
            identifier="timeslot",
            title="Timeslot",
            abstract="Timeslot to process (YYYYMMDDHHMM)",
            type=types.StringType
        )
        self.output_result = self.addLiteralOutput(
            identifier="result",
            title="Result status",
            abstract="The status of the process after finishing the "
                     "processing.",
            type=types.StringType
        )
        self.output_details = self.addLiteralOutput(
            identifier="details",
            title="Result execution details",
            abstract="Additional details pertaining to package execution.",
            type=types.StringType
        )

    def execute(self):
        raise NotImplementedError

    def _update_status(self, message='', percentage=None):
        """Write the new status to the executionResponse document"""
        percentage = percentage or self._previous_percent_done
        self.status.set(msg=message, percentDone=percentage)
        self._previous_percent_done = percentage

    def _test_method(self):
        """This is just a test method"""

        import time
        total_steps = 10.0
        current_step = 0
        while current_step < total_steps:
            percentage = (current_step * 100) / total_steps
            self.status.set(msg='zzz...', percentDone=percentage)
            self._update_status(message='zzz...', percentage=percentage)
            time.sleep(1)
            current_step += 1
        self.output_result.setValue('Done')
        self.output_details.setValue('Just slept for {} seconds'.format(
            total_steps))
