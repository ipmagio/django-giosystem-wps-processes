"""
Configure pywps and the necessary settings for giosystem

This script will create the pywps_settings.cfg file containing settings for PyWPS.
The file will be created in the django project's base path.

It depends on the existance of the following settings:

(The variables mentioned bellow should be set in the 
"giosystem_django_base/settings_local.py" file.)

    BASE_DIR (django defines it by default when the project is created.)

    GIOSYSTEM_SETTINGS_URL ("http://<server_domain_name_for_giosystem_settings>\
/giosystem/coresettings/api/v1/")

"""

import os
import sys
import socket
import shutil
from ConfigParser import ConfigParser

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings as django_settings

import giosystemcore.settings
import giosystemcore.hosts.hostfactory as hf

class Command(BaseCommand):
    can_import_settings = True
    args = ""
    help = __doc__

    def handle(self, *args, **options):
        settings_path = self._copy_template()
        self._customize_settings(settings_path)

    def _copy_template(self):
        template_name = "pywps_settings.cfg.example"
        template_root = os.path.dirname(__file__).rpartition("/")[0]
        sys.stdout.write("template_root: {}\n".format(template_root))
        template_path = os.path.join(template_root, template_name)
        settings_path = os.path.join(django_settings.BASE_DIR, 
                                     os.path.splitext(template_name)[0])
        shutil.copy(template_path, settings_path)
        return settings_path

    def _customize_settings(self, settings_path):
        giosystemcore.settings.get_settings(
                django_settings.GIOSYSTEM_SETTINGS_URL)
        gio_host = hf.get_host()
        #server_name = ''.join((socket.gethostname(), getattr(
        #                      django_settings, "DOMAIN_NAME", '')))
        domain = getattr(django_settings, "ALLOWED_HOSTS", [])
        domain_name = domain[0] if len(domain) > 0 else ''
        server_name = ''.join((socket.gethostname(), domain_name))
        s = django_settings.SITE_SUB_URI
        sub_uri = "/{}".format(s) if s is not None else ""
        server_address = "http://{}{}/wps".format(server_name, sub_uri)
        output_url = '/'.join((server_address, "wpsoutputs"))
        temp_path = os.path.join(gio_host.data_dir, 'WPS_TEMP')
        if not os.path.isdir(temp_path):
            os.makedirs(temp_path)
        here = os.path.abspath(__file__)
        processes_path = here.rsplit('/', 3)[0]
        sys.stdout.write("processes_path: {}".format(processes_path))
        config = ConfigParser()
        config.read(settings_path)
        settings = {
            'wps': {
                "serveraddress": server_address,
            },
            'provider': {},
            'server': {
                'temppath': temp_path,
                'processespath': processes_path,
                'outputurl': output_url,
                'outputpath': temp_path,
            },
            'grass': {},
            'mapserver': {},
        }
        venv_root = os.environ.get('VIRTUAL_ENV', False)
        if venv_root:
            settings['server']['virtualenvpath'] = venv_root
        for section, params in settings.iteritems():
            for param, value in params.iteritems():
                config.set(section, param, value)
        with open(settings_path, 'w') as fh:
            config.write(fh)
