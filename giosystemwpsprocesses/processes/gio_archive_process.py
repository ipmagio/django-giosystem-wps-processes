"""A process to execute giosystem archiving tasks."""

import logging
import datetime as dt

import giosystemcore.settings
import giosystemcore.packages.packagefactory as pf
from giosystemwpsprocesses import baseprocess


logger = logging.getLogger(__name__)


class Process(baseprocess.BaseProcess):

    def __init__(self):
        baseprocess.BaseProcess.__init__(
            self,
            identifier="gio_archive_process",
            title="GIO Archive Process",
            version="0.1",
            abstract="This process archives giosystem packages' outputs."
        )

    def execute(self):
        giosystemcore.settings.get_settings(self.settings_url.getValue())
        timeslot = dt.datetime.strptime(self.timeslot.getValue(), "%Y%m%d%H%M")
        package = pf.get_package(self.package_name.getValue(), timeslot)
        package.progress_callback = self._update_status
        package.use_archive_for_searching = False
        package.use_io_buffer_for_searching = False
        try:
            result, details = package.archive()
        except IOError as err:
            result = False
            details = str(err)
        self.output_result.setValue(result)
        self.output_details.setValue(details)
