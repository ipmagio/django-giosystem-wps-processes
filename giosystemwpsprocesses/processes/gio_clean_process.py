"""A process to execute giosystem cleaning tasks."""

from __future__ import division
import logging
import datetime

import giosystemcore.settings
import giosystemcore.packages.packagefactory as pf
from giosystemwpsprocesses import baseprocess


logger = logging.getLogger(__name__)


class Process(baseprocess.BaseProcess):

    def __init__(self):
        baseprocess.BaseProcess.__init__(
            self,
            identifier="gio_clean_process",
            title="GIO Clean Process",
            version="0.1",
            abstract="This process cleans giosystem packages' outputs."
        )

    def execute(self):
        giosystemcore.settings.get_settings(self.settings_url.getValue(),
                                            initialize_logging=False)
        timeslot = datetime.datetime.strptime(
            self.timeslot.getValue(), "%Y%m%d%H%M")
        package = pf.get_package(self.package_name.getValue(), timeslot)
        package.progress_callback = self._update_status
        percentage = 0
        try:
            progress_step = 100 / len(package.outputs)
        except ZeroDivisionError:
            progress_step = 100
        for output in package.outputs:
            if output.timeslot is not None:
                params = package._outputs[output]
                to_clean = params["should_be_cleaned"]
                days_delta = datetime.timedelta(days=params["cleaning_offset"])
                if to_clean:
                    new_timeslot = package.timeslot - days_delta
                    output.timeslot = new_timeslot
                    package._delete_file(
                        output,
                        must_be_in_archive=params["should_be_archived"]
                    )
                percentage += progress_step
        self.output_result.setValue(True)
        self.output_details.setValue("")
