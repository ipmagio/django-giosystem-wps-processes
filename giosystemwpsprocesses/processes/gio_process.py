"""A process to execute giosystem tasks.

Please note that all logging messages that are printed by this module must have
a level of logging.ERROR to ensure they are written to apache log file
"""

import logging
import types

from pywps import Exceptions

from giosystemwpsprocesses import baseprocess
import giosystemcore.settings
import giosystemcore.errors
import giosystemcore.packages.packagefactory as pf


logger = logging.getLogger(__name__)


class Process(baseprocess.BaseProcess):

    def __init__(self):
        baseprocess.BaseProcess.__init__(
            self,
            identifier="gio_process",
            title="GIO Process",
            version="0.1",
            abstract="This process executes giosystem packages"
        )
        self.copy_outputs_to_archive = self.addLiteralInput(
            identifier="copy_outputs_to_archive",
            title="Copy outputs to archive",
            abstract="Should the package's outputs be copied to the "
                     "archive after execution?",
            type=types.BooleanType
        )
        self.copy_outputs_to_io_buffer = self.addLiteralInput(
            identifier="copy_outputs_to_io_buffer",
            title="Copy outputs to io buffer",
            abstract="Should the package's outputs be copied to the "
                     "io buffer after execution?",
            type=types.BooleanType
        )
        self.remove_package_working_dir = self.addLiteralInput(
            identifier="remove_package_working_dir",
            title="Remove package working directory",
            abstract="Should the package's temporary directory be deleted "
                     "after execution?",
            type=types.BooleanType
        )
        self.use_archive_for_searching = self.addLiteralInput(
            identifier="use_archive_for_searching",
            title="Use archive for searching",
            abstract="Should the package look in the archive when "
                     "searching for missing inputs?",
            type=types.BooleanType
        )
        self.use_io_buffer_for_searching = self.addLiteralInput(
            identifier="use_io_buffer_for_searching",
            title="Use io buffer for searching",
            abstract="Should the package look in the io buffer when "
                     "searching for missing inputs?",
            type=types.BooleanType
        )
        self.extra_hosts_to_copy_outputs = self.addLiteralInput(
            identifier="extra_hosts_to_copy_outputs",
            title="Extra hosts to copy outputs",
            abstract="Other hosts that should receive the outputs"
                     "after execution",
            type=types.StringType
        )

    def execute(self):
        giosystemcore.settings.get_settings(self.settings_url.getValue(),
                                            initialize_logging=False)
        package = pf.get_package(self.package_name.getValue(),
                                 self.timeslot.getValue())
        package.progress_callback = self._update_status
        package.copy_outputs_to_archive = \
            self.copy_outputs_to_archive.getValue()
        package.copy_outputs_to_io_buffer = \
            self.copy_outputs_to_io_buffer.getValue()
        package.remove_working_directory = \
            self.remove_package_working_dir.getValue()
        package.use_archive_for_searching = \
            self.use_archive_for_searching.getValue()
        package.use_io_buffer_for_searching = \
            self.use_io_buffer_for_searching.getValue()
        additional_hosts = self.extra_hosts_to_copy_outputs.getValue()
        extra_hosts = [n.strip() for n in additional_hosts.split(",")
                       if n != ""]
        package.extra_hosts_to_copy_outputs.extend(extra_hosts)
        try:
            result, details = package.run()
        except giosystemcore.errors.ProcessingError as err:
            logger.error(str(err))
            result = False
            details = "{} - {}".format(package.name, str(err))
            raise Exceptions.ServerError(details)
        self.output_result.setValue(result)
        self.output_details.setValue(details)
