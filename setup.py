import os
from setuptools import setup, find_packages

README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()
VERSION = open(os.path.join(os.path.dirname(__file__),
                            'VERSION')).read().strip()

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-giosystem-wps-processes',
    version=VERSION,
    packages=find_packages(),
    install_requires=['django', 'giosystemcore', 'pywps'],
    include_package_data=True,
    license='BSD License',
    description='A Django app that contains WPS processes for giosystem.',
    long_description=README,
    url='',
    author='Ricardo Garcia Silva',
    author_email='ricardo.silva@ipma.pt',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License', # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
